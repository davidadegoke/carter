<!DOCTYPE html>
<html>
<head>
<title>City Guide Properties</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->

<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mattress Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'><!--//fonts-->
<!-- start menu -->
<link href="css/memenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/memenu.js"></script>
<script>$(document).ready(function(){$(".memenu").memenu();});</script>
<script src="js/simpleCart.min.js"> </script>
</head>
<body>
<style type="text/css">
	.container{
		width: 80%;
	}
	.login-pane{
		margin-left: 23%;
	}
	.btn-submit{
		margin-left: 80%;
	}
</style>
<!--header-->
<div class="header">
	<div class="header-top">
		<div class="container">
			<div class="social">
				<ul>
					<li><a href="#"><i class="facebok"> </i></a></li>
					<li><a href="#"><i class="twiter"> </i></a></li>
					<li><a href="#"><i class="inst"> </i></a></li>
					<li><a href="#"><i class="goog"> </i></a></li>
						<div class="clearfix"></div>	
				</ul>
			</div>
			<div class="header-left">
			
				<div class="search-box">
				<!-- search button -->
					<div id="sb-search" class="sb-search">
						<form>
							<input class="sb-search-input" placeholder="Enter to search site..." type="search"  id="search">
							<input class="sb-search-submit" type="submit" value="">
							<span class="sb-icon-search"> </span>
						</form>
					</div>
				</div>
			
<!-- search-scripts -->
					<script src="js/classie.js"></script>
					<script src="js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
					<!-- //search-scripts -->

				<div class="ca-r">
					<div class="cart box_1">
						<a href="checkout.html">
						<h3> <div class="total">
							<span class="simpleCart_total"></span> </div>
							<img src="images/cart.png" alt=""/></h3>
						</a>
						<p><a href="javascript:;" class="simpleCart_empty">Delete Cart</a></p>

					</div>
				</div>
					<div class="clearfix"> </div>
			</div>
				
		</div>
		</div>
		<div class="container">
			<div class="head-top">
				<div class="logo">
					<h1><a href="index.html">City Guide</h1>
					<h3>Quest for Properties </h3></a>
				</div>
		  <div class=" h_menu4">
				<ul class="memenu skyblue">
					  <li><a class="color8" href="index.html">HOME</a></li>	
				      <li><a class="color1" href="#">SERVICES</a>
				      	<div class="mepanel">
						<div class="row">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><h4>Sells</h4></li>
										<li><a href="products.html">Building</a></li>
										<li><a href="products.html">Incomplete Building</a></li>
										<li><a href="products.html">Filling station</a></li>
										<li><a href="products.html">Hectares of Land</a></li>
										<li><a href="products.html">Acres of Land</a></li>
										
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><h4>To Lease</h4></li>
										<li><a href="products.html">Openspace</a></li>
										<li><a href="products.html">Land 50 X 100</a></li>
										<li><a href="products.html">Land 100 X 100</a></li>
										<li><a href="products.html">Land 100 X 120</a></li>
										<li><a href="products.html">Land 100 X 150</a></li>
										<li><a href="products.html">Land 100 X 200</a></li>
										<li><a href="products.html">Land 100 X 300</a></li>
									
										
									</ul>	
								</div>							
							</div>
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><h4>Rents</h4></li>
										<li><a href="products.html">Office Accommodation</a></li>
										<li><a href="products.html">Ware House</a></li>
										<li><a href="products.html">Stores</a></li>
										<li><a href="products.html">Bungalows</a></li>
										<li><a href="products.html">Duplex</a></li>
										<li><a href="products.html">Bed Sitter</a></li>
										<li><a href="products.html">Two Bed rooms Flat</a></li>
										<li><a href="products.html">Three Bed rooms Flat</a></li>
										<li><a href="products.html">Four bed rooms Flat</a></li>
										<li><a href="products.html">Single room</a></li>
										<li><a href="products.html">self contained</a></li>
										<li><a href="products.html">Two rooms</a></li>
										
									</ul>	
								</div>												
							</div>
						  </div>
						</div>
					</li>

					  	
				<li><a class="color4" href="login.html">Login</a></li>				
				<li><a class="color6" href="contact.html">Contact Us</a></li>
			  </ul> 
			</div>
				
				<div class="clearfix"> </div>
		</div>
		</div>
	</div>
	<!-- grow -->
	<div class="grow">
		<div class="container">
			<h2>Login</h2>
		</div>
	</div>
	<!-- grow -->
<!--content-->
<div class="container login-pane">
		<div class="account">
		<div class="account-pass">
		<div class="col-md-8 account-top">
			<form class="container">
				
			<div> 	
				<span>Admin ID</span>
				<input type="text"> 
			</div>
			<div> 
				<span>Admin Passcode</span>
				<input type="password">
			</div>				
				<input class="btn-submit" type="submit" value="Login"> 
			</form>
		</div>
		
<div class="clearfix"> </div>
		</div>
	<div class="clearfix"> </div>
	</div>
	</div>

</div>

<!--//content-->
<div class="footer">
				<div class="container">
			<div class="footer-top-at">
			
				<div class="col-md-3 amet-sed">
				<h4>MORE INFO</h4>
				<ul class="nav-bottom">
						<li><a href="#">About us</a></li>
						<li><a href="#">How to order</a></li>
						<li><a href="#">FAQ</a></li>
						<li><a href="contact.html">Location</a></li>
						<li><a href="#">Terms Of Use</a></li>
						<li><a href="#"></a></li>	
					</ul>	
				</div>
				<div class="col-md-3 amet-sed">
					<h4>CATEGORIES</h4>
					<ul class="nav-bottom">
						<li><a href="#">Services</a></li>
						<li><a href="#">Why Use City Guide</a></li>
						<li><a href="#">Advertise</a></li>
						<li><a href="#">Advice</a></li>
						<li><a href="#">Contact Us</a></li>	
					</ul>
					
				</div>
				<div class="col-md-3 amet-sed">
					<h4>NEWSLETTER</h4>
					<div class="stay">
						<div class="stay-left">
							<form>
								<input type="text" placeholder="Enter your email " required="">
							</form>
						</div>
						<div class="btn-1">
							<form>
								<input type="submit" value="Subscribe">
							</form>
						</div>
							<div class="clearfix"> </div>
			</div>
					
				</div>
				<div class="col-md-3 amet-sed ">
				<h4>CONTACT US</h4>
				<p>Head Office Address</p>
					<p>54 Osadebay-Way,Asaba Delta State, Nigeria.</p>
					<p>Email:<a href="mailto:contact@example.com">Cityguideproperties<br>@yahoo.com</a></p>
					<p>office : +234-9050386983,<br>+234-9090945539,<br>+234-8088679099,<br>+234-8034190349.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="footer-class">
		<p>© 2016 City Guide Properties . All Rights Reserved | Designed by Chipz & Syndicate Technologies </p>
		</div>
		</div>
</body>
</html>