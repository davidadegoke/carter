<?php
 
if(isset($_POST['email'])) {
 
     
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
 
    $email_to = "davidadegoke31@gmail.com";
 
    $email_subject = "Carter Contact Form";
 
     
 
     
 
    function died($error) {
 
        // your error code can go here
 
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
 
        echo "These errors appear below.<br /><br />";
 
        echo $error."<br /><br />";
 
        echo "Please go back and fix these errors.<br /><br />";
 
        die();
 
    }
 
     
 
    // validation expected data exists
 
    if(!isset($_POST['fullname']) ||
 
        !isset($_POST['email']) ||

        !isset($_POST['message'])) {
 
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }
 
     
 
    $fullname = $_POST['fullname']; // required

    $email_from = $_POST['email']; // required

    $address = $_POST['address']; // required

    $phone = $_POST['phone']; // required
 
    $message = $_POST['message']; // required

    
     
 
    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
 
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
 
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$fullname)) {
 
    $error_message .= 'The Name you entered does not appear to be valid.<br />';
 
  }
 
  if(strlen($message) < 2) {
 
    $error_message .= 'The Message you entered do not appear to be valid.<br />';
 
  }
 
  if(strlen($error_message) > 0) {
 
    died($error_message);
 
  }
 
    $email_message = "Form details below.\n\n";
 
     
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }
 
     
 
    $email_message .= "Full Name: ".clean_string($fullname)."\n";

    $email_message .= "Address: ".clean_string($address)."\n";
 
    $email_message .= "Email: ".clean_string($email_from)."\n";

    $email_message .= "Phone: ".clean_string($phone)."\n";

    $email_message .= "Message: ".clean_string($message)."\n";
 
     
 
     
 
// create email headers
 
$headers = 'From: '.$email_from."\r\n".
 
'Reply-To: '.$email_from."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  
 
?>
 
 
<?php
  
  echo "<script type='text/javascript'>Thank you! Your message was sent successfully</script>";
 
 ?>
 
<?php
 
}
 
?>
<!DOCTYPE html>
<html>
<head>
<title>City Guide Properties</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->

<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mattress Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'><!--//fonts-->
<!-- start menu -->
<link href="css/memenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="js/memenu.js"></script>
<script>$(document).ready(function(){$(".memenu").memenu();});</script>
<script src="js/simpleCart.min.js"> </script>
</head>
<body>
<?php include('includes/header.html') ?>
	<!-- grow -->
	<div class="grow">
		<div class="container">
			<h2>Contact</h2>
		</div>
	</div>
	<!-- grow -->
<!--content-->
<div class="contact">
<h4 align="center">Got any Questions or Comments?  Here we can help</h4>
			
			<div class="container">
			<div class="contact-form">
				
				<div class="col-md-8 contact-grid">
					<form method="post" action="contact.php">	
						<input type="text" value="FullName" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='FullName';}" name="fullname">
					
						<input type="text" value="Your Email" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='your Email';}" name="email">
						<input type="text" value="Your Address" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Your Address';}" name="address">
						<input type="text" value="Phone Number" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Phone Number';}" name="phone">
						
						<textarea cols="77" rows="6" value=" " onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Message';}" name="message">Message</textarea>
						<div class="send">
							<input type="submit" value="Send">
						</div>
					</form>
				</div>
				<div class="col-md-4 contact-in">

						<div class="address-more">
						<h4>Address</h4>
							<p>City Guide Properties Ltd,</p>
							<p>Head Office Address,</p>
					<p>54 Osadebay-Way,Asaba Delta State, Nigeria.</p>
					
					
						</div>
						<div class="address-more">
						<h4>Contact Details</h4>
							<p>Tel:+234-9050386983,<br>+234-9090945539,<br>+234-8088679099,<br>+234-8034190349.</p>
						
							<p>Email:<a href="mailto:contact@example.com">Cityguideproperties@gmail.com</a></p>
						</div>
					
				</div>
				<div class="clearfix"> </div>
			</div>
			
		
	</div>
<!--//content-->
<?php include('includes/footer.html') ?>

</body>
</html>